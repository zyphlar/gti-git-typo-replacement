# GTI git typo replacement

Rewards you with sweet Volkswagen-inspired ASCII art when you mistype `git`

## Installation

- Copy `gti` to `/bin/gti`
- Make it executable with `chmod +x /bin/gti`

## License

    Copyright (c) 2023, zyphlar
    All rights reserved.
    
    This program is licensed under the Chicken Dance License v0.2
    https://github.com/supertunaman/cdl

    ASCII Art by [Jaap Peters](https://ascii.co.uk/art/volkswagen)
